apiVersion: v1
kind: ServiceAccount
metadata:
  name: jobtechdev-se
---
# Service to expose and loadbalance between pods.
apiVersion: v1
kind: Service
metadata:
  name: jobtechdev-se
spec:
  selector:
    app: jobtechdev-se
  ports:
    - protocol: TCP
      port: 80
      targetPort: 8080
---
# Deployment to create necessary pods with configurations.
apiVersion: apps/v1
kind: Deployment
metadata:
  name: jobtechdev-se
  labels:
    app: jobtechdev-se
spec:
  replicas: 3
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxUnavailable: 1
  selector:
    matchLabels:
      app: jobtechdev-se
  template:
    metadata:
      labels:
        app: jobtechdev-se
    spec:
      # Pod to launch the service.
      serviceAccountName: jobtechdev-se
      securityContext:
          runAsNonRoot: true
      affinity:
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
          - weight: 1
            podAffinityTerm:
              labelSelector:
                matchLabels:
                  app: jobtechdev-se
              topologyKey: kubernetes.io/hostname
      initContainers:
      - name: init-volume
        image: registry.gitlab.com/arbetsformedlingen/www/jobtechdev-se/main:latest
        imagePullPolicy: Always
        command: ["/bin/init-volume.sh"]
        volumeMounts:
        - mountPath: /mnt
          name: html
        securityContext:
          readOnlyRootFilesystem: true
        resources:
          limits:
            memory: "256Mi"
          requests:
            memory: "256Mi"
      containers:
      - name: jobtech-se
        image: registry.gitlab.com/arbetsformedlingen/www/jobtechdev-se/main:latest
        imagePullPolicy: Always
        ports:
        - containerPort: 8080
        volumeMounts:
        - mountPath: /tmp
          name: tmp
        - mountPath: /var/www/html/tmp
          name: tmp
        - mountPath: /var/run
          name: run
        - mountPath: /var/www/html/cache
          name: cache
        - mountPath: /var/www/html/backup
          name: backup
        - mountPath: /var/www/html/assets
          name: assets
        - mountPath: /var/www/html/images
          name: images
        - mountPath: /var/www/html/sessions
          subPath: sessions
          name: html
        - mountPath: /var/www/html/logs
          subPath: logs
          name: html
        - mountPath: /var/www/html/user/pages
          subPath: user/pages
          name: html
        - mountPath: /var/www/html/user/data
          subPath: user/data
          name: html
        - mountPath: /var/www/html/user/accounts
          subPath: user/accounts
          name: html
        - mountPath: /var/www/html/user/config/plugins/email.yaml
          subPath: email.yaml
          name: email
        readinessProbe:
          initialDelaySeconds: 5
          timeoutSeconds: 5
          httpGet:
            path: /sv
            port: 8080
        livenessProbe:
          initialDelaySeconds: 5
          failureThreshold: 2
          timeoutSeconds: 5
          httpGet:
            path: /sv
            port: 8080
        securityContext:
          readOnlyRootFilesystem: true
        resources:
          limits:
            memory: "1Gi"
          requests:
            memory: "1Gi"
      volumes:
      - name: run
        emptyDir: {}
      - name: tmp
        emptyDir: {}
      - name: cache
        emptyDir: {}
      - name: backup
        emptyDir: {}
      - name: assets
        emptyDir: {}
      - name: images
        emptyDir: {}
      - name: html
        persistentVolumeClaim:
          claimName: html
      - name: email
        secret:
          secretName: email
