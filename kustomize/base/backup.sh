#!/bin/bash -x
# This script requires the following environment variables set:
# * MC_HOST_MINIO - In format https://<Access Key>:<Secret Key>@<YOUR-S3-ENDPOINT>
# * ENV - Name of environment

set -x

BACKUP_DATE=$(date +%F-%R)

cd /var/www/html/

mkdir -p backup

tar cvzf backup/backup.tgz --exclude=user/config/* \
    --exclude=user/plugins/* --exclude=user/themes/* user

# Delete backups older than 30 days on S3.
mc rm -r --force --older-than 30d  minio/backup/runmm-lab/MINIO/backup/jobtechdev-se/${ENV:-unknown}/

# Copy backup to S3
mc cp /var/www/html/backup/backup.tgz MINIO/backup/jobtechdev-se/${ENV:-unknown}/backup-${BACKUP_DATE}.tgz
